# ESP32 Bluetooth Music Sender

# Introduction

In this project, we will be trying to use an ESP32 and a SD Card reader to transmit music to a bluetooth speaker.

# Features

1. USB Powered
2. Music Player mode
   1. Iterate through SD card for `.mp3` files and play them in sequence
3. File Manager Mode
   1. Accessible over HTTP
   2. Allows upload/download/delete of files
   3. Files are stored directly on the SD Card
4. Modify `config.txt` to change Bluetooth speaker name

# Wiring Pinout

| SPI Signal | ESP32 Pin |
| ---------- | --------- |
| MOSI       | 23        |
| MISO       | 19        |
| SCLK       | 18        |
| SS         | 5         |

# Formatting the SD Card

Just to ensure the SD card is formatted into the correct type, I used this codes.

[https://github.com/greiman/SdFat/blob/master/examples/SdFormatter/SdFormatter.ino](https://github.com/greiman/SdFat/blob/master/examples/SdFormatter/SdFormatter.ino)

**Sandisk SD Card Issues**

There seems to be a lot of fake Sandisk SD Cards that doesn’t follow the SD specifications. In order to still be able to use the cards, we have to lower the clock speed when performing the formatting.

```cpp
#define SPI_CLOCK SD_SCK_MHZ(4)

// Try to select the best SD card configuration.
#if HAS_SDIO_CLASS
#define SD_CONFIG SdioConfig(FIFO_SDIO)
#elif  ENABLE_DEDICATED_SPI
#define SD_CONFIG SdSpiConfig(SD_CS_PIN, DEDICATED_SPI, SPI_CLOCK)
#else  // HAS_SDIO_CLASS
#define SD_CONFIG SdSpiConfig(SD_CS_PIN, SHARED_SPI, SPI_CLOCK)
#endif  // HAS_SDIO_CLASS
```

# References

ESP8266 Library - [https://github.com/earlephilhower/ESP8266Audio](https://github.com/earlephilhower/ESP8266Audio)
