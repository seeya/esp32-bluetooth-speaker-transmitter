#include <Arduino.h>
#include <SPI.h>
#include <SD.h>
#include "SPIFFS.h"
#include "AudioGeneratorMP3.h"
#include "BluetoothA2DPSource.h"
#include "AudioTools.h"
#include "AudioLibs/AudioESP8266.h"
#include "AudioFileSourceSD.h"
#include <OneButton.h>
#include <WiFiManager.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Int64String.h>

#define BUTTON_PIN 0
File dir;
File uploadFile;
BluetoothA2DPSource a2dp_source;
AudioFileSourceSD *source = NULL;
AudioOutputWithCallback *output = NULL;
AudioGeneratorMP3 *decoder = NULL;
WiFiManager wm;
WebServer server(80);
bool fileBrowserMode = false;
String bluetoothSpeakerName = "";

const int sd_ss_pin = 5;
volatile float volume = .5;

OneButton btn = OneButton(
  BUTTON_PIN,  // Input pin for the button
  true,        // Button is active LOW
  true         // Enable internal pull-up resistor
);

void stopBluetooth();
void startBluetooth();
void initWebServer();
void setupWifi();
String listDirectory();

String getContentType(String filename) { // convert the file extension to the MIME type
  if(filename.endsWith(".htm")) return "text/html";
  else if(filename.endsWith(".html")) return "text/html";
  else if(filename.endsWith(".css")) return "text/css";
  else if(filename.endsWith(".js")) return "application/javascript";
  else if(filename.endsWith(".png")) return "image/png";
  else if(filename.endsWith(".gif")) return "image/gif";
  else if(filename.endsWith(".jpg")) return "image/jpeg";
  else if(filename.endsWith(".ico")) return "image/x-icon";
  else if(filename.endsWith(".xml")) return "text/xml";
  else if(filename.endsWith(".pdf")) return "application/x-pdf";
  else if(filename.endsWith(".zip")) return "application/x-zip";
  else if(filename.endsWith(".gz")) return "application/x-gzip";
  else if(filename.endsWith(".mp3")) return "audio/mpeg";
  else if(filename.endsWith(".mp4")) return "audio/mp4";
  else if(filename.endsWith(".wave")) return "audio/vnd.wav";
  else if(filename.endsWith(".ico")) return "image/x-icon";
  return "text/plain";
}

bool handleFileRead(String path) {
  String contentType = getContentType(path);
  String pathWithGz = path + ".gz";

  if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)) {
    if (SPIFFS.exists(pathWithGz)) path += ".gz";
    
    File file = SPIFFS.open(path, "r");
    size_t sent = server.streamFile(file, contentType);

    file.close();
    return true;
  }
  Serial.println(String("\tFile Not Found: ") + path);
  return false;
}

bool handleSDFileRead(String path) {
  String contentType = getContentType(path);

  if (SD.exists(path)) {
    
    File file = SD.open(path, "r");
    size_t sent = server.streamFile(file, contentType);

    file.close();
    return true;
  }
  Serial.println(String("\tFile Not Found: ") + path);
  return false;
}

void setupWifi() {
  fileBrowserMode = true;
  WiFi.mode(WIFI_STA); 
  wm.autoConnect("WifiFileExplorer","password123");

  initWebServer();
}

void getBluetoothSpeakerName() {
  String config_path = "/config.txt";

  if(SD.exists(config_path)) {
    File config = SD.open(config_path);
    bluetoothSpeakerName = config.readString();
    Serial.println("Found Bluetooth Name: " + bluetoothSpeakerName);
    config.close();
  }
} 

void initWebServer() {
  SPIFFS.begin();

  server.on("/", HTTP_GET, []() {
    if (!handleFileRead("/upload.html")) { 
      server.send(404, "text/plain", "404: Not Found"); 
    }
  });

  server.on("/favicon.ico", HTTP_GET, []() {
    if (!handleFileRead("/favicon.ico")) { 
      server.send(404, "text/plain", "404: Not Found"); 
    }
  });

  server.on("/app/sdcard/info", HTTP_GET, []() {
    String card_type = String(SD.cardType());
    String used_bytes = int64String(SD.usedBytes());
    String total_bytes = int64String(SD.totalBytes());

    server.send(200, "/text/plain", card_type + ";" + used_bytes + ";" + total_bytes + ";" + bluetoothSpeakerName);
  });

  server.on("/app/sdcard/list", HTTP_GET, []() {
    server.sendHeader("Access-Control-Allow-Origin", "*");
    server.send(200, "text/plain", listDirectory());
  });

  server.on("/app/sdcard/dl", HTTP_GET, [&]() {
    String file_name = "/" + server.arg("file_name");

    if(!handleSDFileRead(file_name)) {
      server.send(404, "text/plain", "404: Not Found"); 
    }
  });

  server.on("/app/sdcard/config", HTTP_POST, [&]() {
    server.send(200, "text/plain", "Config updated");
    String file_name = "/config.txt";

    Serial.println("Updating bluetooth speaker name: " + server.arg(0));

    File config = SD.open(file_name, "w");
    config.print(server.arg(0));
    config.close();
  });

  server.on("/app/sdcard/delete", HTTP_POST, [&]() {
    server.send(200, "text/plain", "File deleted");

    String file_name = "/" + server.arg(0);
    Serial.println("Deleting File: " + file_name);
    if(SD.exists(file_name)) SD.remove(file_name);
  });

  server.on("/app/sdcard/upload", HTTP_POST, [&]() {}, [&]() {
    HTTPUpload& upload = server.upload();

    if (upload.status == UPLOAD_FILE_START) {
      String filename = upload.filename;

      if (!filename.startsWith("/")) {
        filename = "/" + filename;
      }

      uploadFile = SD.open(filename, "w");
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      if(uploadFile) {
        uploadFile.write(upload.buf, upload.currentSize);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      if (uploadFile) { 
        Serial.println("Wrote: " + String(upload.totalSize));
        uploadFile.close(); 
        server.send(200, "text/plain", "File saved on SD Card");
      } else {
        Serial.println("[File not saved]");
        server.send(200, "text/plain", "Error writing to file!");
      }

      listDirectory();
    }
  });

  server.begin();

  // http://esp32bt.local
  if (!MDNS.begin("esp32bt")) {
    Serial.println("Error setting up MDNS responder!");
    while(1) {
        delay(1000);
    }
  }
  MDNS.addService("http", "tcp", 80);

}

String listDirectory() {
  String files = "";
  dir.rewindDirectory();
  File f = dir.openNextFile();

  f.size();

  while(f) {
    files += String(f.name()) + ";" + String(f.size());
    f = dir.openNextFile();

    if(f) files += ",";
  }

  return files;
}

int32_t get_sound_data(Frame* data, int32_t len) {  
  if (output == nullptr) return 0;
  int32_t result = output->read(data, len);

  for (int j=0;j<len;j++){
    data[j].channel1 = data[j].channel1 * volume;
    data[j].channel2 = data[j].channel2 * volume;
  }
  return result;
}

void bt_callback(esp_a2d_connection_state_t state, void *ptr) {
  Serial.println(a2dp_source.to_str(state));

  if(strcmp(a2dp_source.to_str(state), "Connected") == 0) {
    Serial.println("isBTConnected = true");
    delay(1000);
  } else if(strcmp(a2dp_source.to_str(state), "Disconnected") == 0){
    Serial.println("isBTConnected = false");
  } else {
    Serial.println("isBTConnected = false");
  }
}

static void handleSingleClick() {
  Serial.println("[Single Click]");
  if(decoder != NULL && decoder->isRunning()) {
    decoder->stop();
  }
}

static void handleDoubleClick() {
  Serial.println("[Double Clicked]");
  // stopBluetooth();
  delay(500);
  setupWifi();
}

static void handleLongPress() {
  Serial.println("[Long Pressed]");
  startBluetooth();
}

void stopBluetooth() {
  Serial.println("[Stopping Bluetooth]");
  a2dp_source.end();

  source->close();
  output->end();
  decoder->stop();

  source = NULL;
  output = NULL;
  decoder = NULL;
}

void startBluetooth() {
  Serial.println("[Connecting to Bluetooth]");
  a2dp_source.set_auto_reconnect(false);
  a2dp_source.set_on_connection_state_changed(bt_callback);
  a2dp_source.start(bluetoothSpeakerName.c_str(), get_sound_data);  

  // Setup Audio
  decoder = new AudioGeneratorMP3();
  output = new AudioOutputWithCallback(512,5);
  source = new AudioFileSourceSD();
}


void initSDCard() {
  Serial.println("Initializing SD card...");

  if (!SD.begin()) {
    Serial.println("Initialization failed!");
    return;
  }
  dir = SD.open("/"); 
  getBluetoothSpeakerName();
  Serial.println("Initialization done.");
}

// Arduino Setup
void setup(void) {
  Serial.begin(115200);

  while(!Serial) {;}

  // Setup Buttons
  btn.attachClick(handleSingleClick);
  btn.attachDoubleClick(handleDoubleClick);
  btn.attachLongPressStart(handleLongPress);

  initSDCard();
}

void loop() {
  btn.tick();

  if(fileBrowserMode) {
    server.handleClient();
    delay(2);
    return;
  }

  if(!a2dp_source.is_connected()) { return; }

  if(decoder->isRunning()) {
    if (!decoder->loop()) {
      Serial.println("[Music ended]");
      decoder->stop();
      delay(1000);
    }
    return;
  } else {
    File file = dir.openNextFile();
    Serial.println("File: " + String(file.name()));

    if(file) {
      if (String(file.name()).endsWith(".mp3")) {
        if(source->isOpen()) source->close();

        String file_path = "/" + String(file.name());
        if (source->open(file_path.c_str())) { 
          Serial.printf_P(PSTR("Playing '%s' from SD card...\n"), file.name());
          decoder->begin(source, output);
        } else {
          Serial.printf_P(PSTR("Error opening '%s'\n"), file.name());
        }
      }
    }
    else {
      Serial.println("[Reach end of directory]");
      dir.rewindDirectory();
      delay(1000);
    }
  }
}